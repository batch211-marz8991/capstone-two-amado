const Product = require("../models/Product");
const Order = require("../models/Order");


//creating a product controller
module.exports.addProduct = (data) => {

	console.log(data);
 
	if(data.isAdmin){

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		console.log(newProduct);
		return newProduct.save().then((product, error) => {

		if (error) {

			return false;

		} else {

			return newProduct;

		};

	});

	}else{
		return false;
	};
};

//retrieve all active products controller
module.exports.getAllActiveProduct = () =>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	});
};

//retrieving all products controller
module.exports.getAllProduct = () =>{
	return Product.find({}).then(result=>{
		return result;
	});
};

//retrieving specific product controller
module.exports.getProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};

//update product information controller
module.exports.updateProduct = (reqParams,reqBody) =>{

	let updatedProduct ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
		
		if(error){
			return false;
		// Product updated successfully
		}else{
			return "Product updated successfully";
		};
	});
};

//Deleting a product
module.exports.deleteProduct = (productId) =>{

	return Product.findByIdAndRemove(productId).then((removedProduct,err)=>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedProduct;
		}
	})
}



//archiving product controller

module.exports.archiveProduct = (reqParams)=>{
	return Product.findByIdAndUpdate(reqParams,{isActive:false}).then((product,error)=>{
		if(error){
			return false
		}else{
			return "Product has been archived"
		}
	});
};

//activating product
module.exports.activateProduct = (reqParams)=>{
	return Product.findByIdAndUpdate(reqParams,{isActive:true}).then((product,error)=>{
		if(error){
			return false
		}else{
			return "Product has been activated"
		}
	});
};